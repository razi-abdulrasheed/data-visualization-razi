$(document).ready(function() {
    $.ajax({
        type: "GET",
        url: "./assets/combinedData.json",
        success: function(Data) {
            console.log(Data);
            Highcharts.chart('chart-1', {
                chart: {
                    type: 'column'
                },
                title: {
                    text: 'Matches played per season over all IPL seasons'
                },
                subtitle: {
                    text: 'Number of matches per year in each IPL seasons'
                },
                xAxis: {
                    type: 'category',
                    labels: {
                        rotation: -45,
                        style: {
                            fontSize: '13px',
                            fontFamily: 'Verdana, sans-serif'
                        }
                    }
                },
                yAxis: {
                    min: 0,
                    title: {
                        text: 'Number of matches'
                    }
                },
                legend: {
                    enabled: false
                },
                tooltip: {
                    pointFormat: 'Number of matches: <b>{point.y}</b>'
                },
                series: [{
                    name: 'Seasons',
                    data: Data.matches,
                    dataLabels: {
                        enabled: true,
                        rotation: -90,
                        color: '#FFFFFF',
                        align: 'right',
                        format: '{point.y}', // one decimal
                        y: 10, // 10 pixels down from the top
                        style: {
                            fontSize: '13px',
                            fontFamily: 'Verdana, sans-serif'
                        }
                    }
                }]
            });

            Highcharts.chart('chart-2', {
                chart: {
                    type: 'bar'
                },
                title: {
                    text: 'Matches won by all teams over all IPL seasons'
                },
                xAxis: {
                    categories: Data.seasons
                },
                yAxis: {
                    min: 0,
                    title: {
                        text: 'Total matches won'
                    }
                },
                legend: {
                    reversed: true
                },
                plotOptions: {
                    series: {
                        stacking: 'normal'
                    }
                },
                series: Data.wins
            });



            Highcharts.chart('chart-3', {
                chart: {
                    type: 'column'
                },
                title: {
                    text: 'Extra runs conceded per team in the IPL season 2016'
                },
                subtitle: {
                    text: 'Extra runs conceded by each team in the IPL season 2016'
                },
                xAxis: {
                    type: 'category',
                    labels: {
                        rotation: -45,
                        style: {
                            fontSize: '13px',
                            fontFamily: 'Verdana, sans-serif'
                        }
                    }
                },
                yAxis: {
                    min: 0,
                    title: {
                        text: 'Extra runs'
                    }
                },
                legend: {
                    enabled: false
                },
                tooltip: {
                    pointFormat: 'conceded <b>{point.y} extra runs in 2016</b>'
                },
                series: [{
                    name: 'Teams',
                    data: Data.extras,
                    dataLabels: {
                        enabled: true,
                        rotation: -90,
                        color: '#FFFFFF',
                        align: 'right',
                        format: '{point.y}', // one decimal
                        y: 10, // 10 pixels down from the top
                        style: {
                            fontSize: '13px',
                            fontFamily: 'Verdana, sans-serif'
                        }
                    }
                }]
            });

            Highcharts.chart('chart-4', {
                chart: {
                    type: 'column'
                },
                title: {
                    text: 'Top economical bowlers in the IPL season 2015'
                },
                subtitle: {
                    text: 'Bowlers who conceded least runs in the IPL season 2015'
                },
                xAxis: {
                    type: 'category',
                    labels: {
                        rotation: -45,
                        style: {
                            fontSize: '13px',
                            fontFamily: 'Verdana, sans-serif'
                        }
                    }
                },
                yAxis: {
                    min: 0,
                    title: {
                        text: 'Economy rate'
                    }
                },
                legend: {
                    enabled: false
                },
                tooltip: {
                    pointFormat: 'conceded <b>{point.y:.1f} runs in each over</b>'
                },
                series: [{
                    name: 'Population',
                    data: Data.economies,
                    dataLabels: {
                        enabled: true,
                        rotation: -90,
                        color: '#FFFFFF',
                        align: 'right',
                        format: '{point.y:.1f}', // one decimal
                        y: 10, // 10 pixels down from the top
                        style: {
                            fontSize: '13px',
                            fontFamily: 'Verdana, sans-serif'
                        }
                    }
                }]
            });
        }
    });
});
