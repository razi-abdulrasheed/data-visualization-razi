var fs = require('fs');
const deliveries = './data-set/deliveries.csv';
const csv = require('csvtojson');
var deliveriesDetailedList = [];

csv({
        workerNum: 4,
        trim: true,
    })
    .fromFile(deliveries)
    .on('csv', (delivery) => {
        deliveriesDetailedList.push(delivery);
    })
    .on('done', (error) => {
        fs.writeFile('./JSON/eliveries.json', JSON.stringify(deliveriesDetailedList), (err) => {
            if (err) throw err;
            console.log('The file has been saved!');
        });
        console.log('\'deliveries.json\' has built successfully.');
    })
