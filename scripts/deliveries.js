var fs = require('fs')
var deliveriesFile = './JSON/deliveries.json'
var matchesFile = './JSOn/matches.json'

fs.readFile(matchesFile, 'utf8', (error, matchesData) => {
    if (error) throw error;
    rawMatchesData = JSON.parse(matchesData);
    var requiredMatchIDs = rawMatchesData.reduce(function(acc, ele) {
        if (ele[1] === '2015') {
            acc['2015'].push(ele[0]);
        } else if (ele[1] === '2016') {
            acc['2016'].push(ele[0]);
        }
        return acc;
    }, {
        '2015': [],
        '2016': []
    });

    fs.readFile(deliveriesFile, 'utf8', function(err, Ddata) {
        if (err) throw err
        DeliveriesData = JSON.parse(Ddata)
        var matchIDs = requiredMatchIDs['2016']
        var extrasList = []
        matchIDs.forEach(e => {
            extrasList.push(DeliveriesData.filter(x => x[0] === e && x[16] != '0'))
        })
        var teamsAndExtras = extrasList.map((e) => {
            return e.reduce((acc, b) => {
                var team = b[3]
                acc[team] = (acc[team] || 0) + Number(b[16])
                return acc
            }, {})
        })

        fs.readFile('./JSON/matchesWonOverEntireIpl.json', 'utf8', function(err, Tdata) {
            if (err) throw err
            var teams = JSON.parse(Tdata)['teams'];
            var extrasPerTeam = teams.map((y) => {
                return teamsAndExtras.reduce((accum, x) => {
                    if (x.hasOwnProperty(y)) {
                        accum[1] += Number(x[y])
                    }
                    return accum;
                }, [y, 0]);
            });

            extrasPerTeam = extrasPerTeam.filter(x => x[1] > 1);

            fs.writeFile('./JSON/extrasPerTeamIn2016.json', JSON.stringify(extrasPerTeam), (err) => {
                if (err) throw err;
                console.log('The extrasPerTeamIn2016.json file has been saved!');
            });

        })

        var allDeliveries = JSON.parse(Ddata);
        var idsOfMatches = requiredMatchIDs['2015'];
        var season2015 = [];
        idsOfMatches.forEach(u => {
            allDeliveries.forEach(p => {
                if (p[0] === u) {
                    season2015.push(p);
                }
            })
        })

        var bowlers = season2015.reduce((bowlers, ball) => {
            if (bowlers.hasOwnProperty(ball[8])) {
                bowlers[ball[8]][0] += Number(ball[17]);
                if (ball[16] === '0') {
                    bowlers[ball[8]][1]++;
                }
                bowlers[ball[8]][2] = bowlers[ball[8]][0] / (bowlers[ball[8]][1] / 6);
            } else {
                bowlers[ball[8]] = [];
                bowlers[ball[8]].push(Number(ball[17]));
                bowlers[ball[8]].push(0);
                if (ball[16] === '0') {
                    bowlers[ball[8]][1]++;
                }
                bowlers[ball[8]].push(0);
                if (bowlers[ball[8]][1] > 0) {
                    bowlers[ball[8]][2] = bowlers[ball[8]][0] / (bowlers[ball[8]][1] / 6);
                }
            }
            return bowlers;
        }, {});

        var bowlersEconomyRate = [];
        for (var player in bowlers) {
            var eachBowler = [];
            if (bowlers.hasOwnProperty(player)) {
                if (bowlers[player][2] < 7.5 && bowlers[player][0] > 120) {
                    eachBowler.push(player);
                    eachBowler.push(bowlers[player][2]);
                    bowlersEconomyRate.push(eachBowler);
                }
            }
        }

        fs.writeFile('./JSON/topEconomyRatesIn2015.json', JSON.stringify(bowlersEconomyRate), (err) => {
            if (err) throw err;
            console.log('The topEconomyRatesIn2015.json file has been saved!');
        });

    });
});
