var fs = require('fs');
var matchesFile = './JSON/matches.json';
var matchesPerYear = new Object;
var matchesWon;
var temp;

fs.readFile(matchesFile, 'utf8', (err, data) => {
    if (err) throw err;
    var rawData = JSON.parse(data);
    var matchesPerYearBulk = rawData.map(l => l[1]);
    var winnerTeamBulk = rawData.map(m => [m[1], m[10]]);
    var teamsNamesBulk = winnerTeamBulk.map(k => k[1]);
    matchesPerYear = matchesPerYearBulk.reduce((count, match) => {
        count[match] = (count[match] || 0) + 1;
        return count;
    }, {});

    let allYears = Object.keys(matchesPerYear);
    var value = Object.values(matchesPerYear);
    var matchesPerYearChartData = allYears.map((e, index) => [allYears[index], value[index]]);
    fs.writeFile('./JSON/matchesPerYearChartData.json', JSON.stringify(matchesPerYearChartData), (err) => {
        if (err) throw err;
        console.log('The matchesPerYearChartData.json file has been saved!');
    });

    var everyIplTeamsObj = teamsNamesBulk.reduce((teams, element) => {
        teams[element] = (teams[element] || 0) + 1;
        return teams;
    }, {});

    var everyIplTeams = Object.keys(everyIplTeamsObj);
    var everyIplTeamsList = everyIplTeams.filter(p => p !== '');

    matchesWon = allYears.map(function(year) {
        return winnerTeamBulk.filter(x => x[0] === year && x[1] !== '')
            .reduce((tally, winsRow) => {
                var winner = winsRow[1];
                tally[winner] = (tally[winner] || 0) + 1;
                return tally;
            }, {});
    });

    var matchesWonChartData = everyIplTeamsList.map(function(currentTeam) {
        return matchesWon.reduce(function(accumulator, wonInTheYear) {
            if (wonInTheYear.hasOwnProperty(currentTeam)) {
                accumulator['data'].push(wonInTheYear[currentTeam]);
            } else {
                accumulator['data'].push(0);
            }
            return accumulator;
        }, {
            name: currentTeam,
            data: []
        })
    });

    var matchesWonOverEntireIpl = new Object;
    matchesWonOverEntireIpl['teams'] = everyIplTeamsList;
    matchesWonOverEntireIpl['seasons'] = allYears;
    matchesWonOverEntireIpl['data'] = matchesWonChartData;

    fs.writeFile('./JSON/matchesWonOverEntireIpl.json', JSON.stringify(matchesWonOverEntireIpl), (err) => {
        if (err) throw err;
        console.log('The matchesWonOverEntireIpl.json file has been saved!');
    });
});
