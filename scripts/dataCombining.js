var fs = require('fs');
var match = './JSON/matchesPerYearChartData.json';
var win = './JSON/matchesWonOverEntireIpl.json';
var extra = './JSON/extrasPerTeamIn2016.json';
var economy = './JSON/topEconomyRatesIn2015.json';
var combinedData = new Object;

fs.readFile( match, 'utf8', (err, match) => {
    if (err) throw err;
    fs.readFile( win, 'utf8', (err, win) => {
        if (err) throw err;
        fs.readFile( extra, 'utf8', (err, extra) => {
            if (err) throw err;
            fs.readFile( economy, 'utf8', (err, economy) => {
                if (err) throw err;

                var matches = JSON.parse(match);
                var won = JSON.parse(win);
                var wins = new Object;
                wins = won.data;
                var seasons = won.seasons;
                var allTeams = new Object;
                allTeams = won.teams;
                var extras = JSON.parse(extra);
                var economies = JSON.parse(economy);

                combinedData['matches'] = matches;
                combinedData['wins'] = wins;
                combinedData['allTeams'] = allTeams;
                combinedData['extras'] = extras;
                combinedData['economies'] = economies;
                combinedData['seasons'] = seasons;

                fs.writeFile('../public/assets/combinedData.json', JSON.stringify(combinedData), (err) => {
                    if (err) throw err;
                    console.log('The combinedData.json file has been saved!');
                });

            })
        })
    })
})

